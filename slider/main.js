(function() {
    var imagesList = [
        'imgs/image1.jpg',  //0
        'imgs/image2.jpg',  //1
        'imgs/image3.jpg',  //2
        'imgs/image4.jpg',  //3
        'imgs/image5.jpg',  //4
        'imgs/image6.jpg'   //5
    ];

    // Execute only once on page is loaded
    var slideIndex = 0;
    var slider;
    var timerId;

    function onInit() {
      slider = document.getElementById('slider');
      
      var img = document.createElement('img');        
      img.setAttribute('src', imagesList[slideIndex] );
      img.className = 'old-img';
      slider.appendChild(img);

      var btnPrev = document.createElement('button');
      btnPrev.setAttribute('class', 'buttonPrev');
      btnPrev.innerHTML = "Prev";
      slider.appendChild(btnPrev);
      document.getElementsByClassName('buttonPrev')[0].addEventListener('click', previousSlide);

      var btnNext = document.createElement('button');
      btnNext.setAttribute('class', 'buttonNext');
      btnNext.innerHTML = "Next";
      slider.appendChild(btnNext);
      document.getElementsByClassName('buttonNext')[0].addEventListener('click', nextSlide);

      startTimer ();  
    }

    // Execute on next button click
    function nextSlide() {
        slideIndex++;

        if (slideIndex > imagesList.length - 1) {
          slideIndex = 0;
        }

        var nextImg = document.createElement('img');
        nextImg.setAttribute( 'src', imagesList[slideIndex] );
        
        var oldImg = document.getElementsByClassName('old-img')[0];
        slider.replaceChild(nextImg, oldImg);
        nextImg.className = 'old-img';
        
        stopTimer ();
        startTimer ();
    }

    // Execute on previous button click
    function previousSlide() {
      slideIndex--;
      
      if (slideIndex < 0) {
        slideIndex = imagesList.length - 1;  
      }

      var prevImg = document.createElement('img');
      prevImg.setAttribute( 'src', imagesList[slideIndex] );

      var oldImg = document.getElementsByClassName('old-img')[0];
      slider.replaceChild(prevImg, oldImg);
      prevImg.className = 'old-img';

      stopTimer ();
      startTimer ();
    }
 
    function startTimer () {
      timerId = setInterval(nextSlide, 3000);  
    }

    function stopTimer () {
      clearTimeout(timerId);  
    }

    document.addEventListener("DOMContentLoaded", onInit);


})();

